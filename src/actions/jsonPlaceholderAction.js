import {
  JSON_PLACEHOLDER_REQUEST,
  JSON_PLACEHOLDER_SUCCESS,
  JSON_PLACEHOLDER_FAIL,
} from '../constants/jsonPlaceholderConstants';
import { getJsonData } from '../utils/api';

export const jsonPlaceholderListAction = (startIndex) => async (dispatch) => {
  try {
    dispatch({ type: JSON_PLACEHOLDER_REQUEST });
    const { data } = await getJsonData(startIndex);

    dispatch({
      type: JSON_PLACEHOLDER_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: JSON_PLACEHOLDER_FAIL,
      payload: 'Something went wrong please try again later.',
    });
  }
};
