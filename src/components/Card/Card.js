import React from 'react';
import './Card.css';

const Card = ({ refs, title, body }) => {
  return (
    <div className='card' ref={refs}>
      <div className='cardTitle'>
        <strong>Title : </strong> {title}
      </div>
      <div className='cardDescription'>
        <strong>Info : </strong> {body}
      </div>
    </div>
  );
};

export default Card;
