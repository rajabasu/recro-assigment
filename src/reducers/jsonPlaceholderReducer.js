import {
  JSON_PLACEHOLDER_REQUEST,
  JSON_PLACEHOLDER_SUCCESS,
  JSON_PLACEHOLDER_FAIL,
} from '../constants/jsonPlaceholderConstants';

export const jsonPlaceholderListReducer = (state = { list: [] }, action) => {
  switch (action.type) {
    case JSON_PLACEHOLDER_REQUEST:
      return { loading: true, list: [] };
    case JSON_PLACEHOLDER_SUCCESS:
      return { loading: false, list: action.payload };
    case JSON_PLACEHOLDER_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
