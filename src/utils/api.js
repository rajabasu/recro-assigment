import axios from 'axios';

const apiUrl = 'https://jsonplaceholder.typicode.com';

const getJsonData = async (startIndex) => {
  try {
    let res = await axios.get(`${apiUrl}/posts?_start=${startIndex}&_limit=10`);

    return res;
  } catch (error) {
    throw error;
  }
};

export { getJsonData };
