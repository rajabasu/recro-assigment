import React, { useState, useEffect, useRef, useCallback } from 'react';
import './Home.css';
import Card from '../../components/Card/Card';
import { useDispatch, useSelector } from 'react-redux';
import { jsonPlaceholderListAction } from '../../actions/jsonPlaceholderAction';
import Loader from '../../components/Loader/Loader';

const Home = () => {
  const dispatch = useDispatch();

  const [jsonList, setJsonList] = useState([]);
  const [startIndex, setStartIndex] = useState(0);

  const jsonPlaceholderList = useSelector((state) => state.jsonPlaceholderList);
  const { loading, error, list } = jsonPlaceholderList;

  const observer = useRef();

  // checking the last element of the list
  const lastListElement = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();

      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting) {
          if (list.length > 0) setStartIndex(startIndex + 10);
        }
      });

      if (node) observer.current.observe(node);
    },
    [loading]
  );

  // storing the prevState and the new state
  useEffect(() => {
    setJsonList([...jsonList, ...list]);
  }, [list]);

  useEffect(() => {
    dispatch(jsonPlaceholderListAction(startIndex));
  }, [dispatch, startIndex]);

  return (
    <div className='homeContainer'>
      <div className='headerTitle'>
        <h1>Recro Assignment Option 1</h1>
      </div>
      <div>
        {jsonList.length > 0 &&
          jsonList.map((items, i) => {
            // condition - if last element pass ref or dont
            if (jsonList.length === i + 1) {
              return (
                <Card
                  key={i}
                  refs={lastListElement}
                  title={items.title}
                  body={items.body}
                />
              );
            } else {
              return <Card key={i} title={items.title} body={items.body} />;
            }
          })}
        {loading ? <Loader /> : null}
        {error ? <>{error}</> : null}
      </div>
    </div>
  );
};

export default Home;
